use std::io::{self, BufRead};

const STRIDE: usize = 3;

fn main() {
    let stdin = io::stdin();
    let rows: Vec<String> = stdin.lock().lines()
        .map(|l| l.unwrap())
        .collect();

    let mut trees = 0;
    for i in 0..rows.len() {
        if rows[i].chars().nth((i * STRIDE) % rows[i].len()).unwrap() == '#' {
            trees += 1;
        }
    }
    println!("Part 1: {}", trees);

    let slopes = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    let mut results = vec![0, 0, 0, 0, 0];
    for (j, (right, down)) in slopes.iter().enumerate() {
        let mut i = 0;
        while i < rows.len() {
            if rows[i].chars().nth((i * (right / down)) % rows[i].len()).unwrap() == '#' {
                results[j] += 1;
            }
            i += down;
        }
    }
    println!("{:?}", results);
    println!("Part 2: {}", results.iter().product::<isize>());
}
