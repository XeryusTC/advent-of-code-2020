use std::io::{self, BufRead};
use std::collections::HashMap;

fn part2(validity: &HashMap<&str, String>) -> bool {
    validity.iter().map(|(key, val)| match *key {
        "byr" => {
            let val = val.parse::<i32>().unwrap();
            val >= 1920 && val <= 2002
        }
        "iyr" => {
            let val = val.parse::<i32>().unwrap();
            val >= 2010 && val <= 2020
        }
        "eyr" => {
            let val = val.parse::<i32>().unwrap();
            val >= 2020 && val <= 2030
        }
        "hgt" => {
            if val.ends_with("cm") {
                let val = val.chars().filter(|c| c.is_numeric()).collect::<String>().parse::<i32>().unwrap();
                val >= 150 && val <= 193
            } else if val.ends_with("in") {
                let val = val.chars().filter(|c| c.is_numeric()).collect::<String>().parse::<i32>().unwrap();
                val >= 59 && val <= 76
            } else {
                false
            }
        }
        "hcl" => {
            val.len() == 7 && val.chars().nth(0).unwrap() == '#'
                && val.chars().skip(1).all(|c| c.is_ascii_hexdigit())
        }
        "ecl" => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&val.as_str()),
        "pid" => val.len() == 9 && val.chars().all(|c| c.is_numeric()),
        _ => true,
    })
        .all(|v| v)
}

fn main() {
    let stdin = io::stdin();
    let data: Vec<String> = stdin.lock().lines()
        .map(|l| l.unwrap())
        .collect();

    let mut validity: HashMap<&str, String> = ["byr", "iyr", "eyr", "hgt", "hcl",
                                             "ecl", "pid"]
        .iter().cloned().map(|e| (e, String::new())).collect();
    let mut valid = 0;
    let mut valid2 = 0;
    for line in data.iter() {
        if line.is_empty() {
            if validity.values().all(|v| !v.is_empty()) {
                valid += 1;
                if part2(&validity) {
                    valid2 += 1;
                }
            }
            validity = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
                .iter().cloned().map(|e| (e, String::new())).collect();
        } else {
            for field in line.split_whitespace() {
                let key = field.split(':').nth(0).unwrap();
                let val = field.split(':').nth(1).unwrap();
                let value = validity.entry(key).or_insert(String::new());
                *value = val.to_string();
            }
        }
    }
    if validity.values().all(|v| !v.is_empty()) {
        valid += 1;
        if part2(&validity) {
            valid2 += 1;
        }
    }
    println!("Part 1: {}", valid);
    println!("Part 2: {}", valid2);
}
