use std::io::{self, BufRead};
use std::collections::HashSet;

fn main() {
    let stdin = io::stdin();
    let data: Vec<String> = stdin.lock().lines().map(|l| l.unwrap()).collect();

    let mut answers: HashSet<char> = HashSet::new();
    let mut counts = 0;
    for line in data.iter() {
        if line.is_empty() {
            counts += answers.len();
            answers = HashSet::new();
        }
        for c in line.chars() {
            answers.insert(c);
        }
    }
    counts += answers.len();
    println!("Part 1: {}", counts);

    let mut answers: HashSet<char> = "qwertyuiopasdfghjklzxcvbnm".chars().collect();
    let mut counts = 0;
    for line in data.iter() {
        if line.is_empty() {
            counts += answers.len();
            answers = "qwertyuiopasdfghjklzxcvbnm".chars().collect();
            continue;
        }
        answers = answers.intersection(&line.chars().collect()).cloned().collect();
    }
    counts += answers.len();

    println!("Part 2: {}", counts)
}