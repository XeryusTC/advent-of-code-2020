use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let passes: Vec<(isize, isize)> = stdin.lock().lines()
        .map(|l| {
            let mut row = 0;
            let mut col = 0;
            for (i, c) in l.unwrap().chars().enumerate() {
                if i < 7 {
                    if c == 'B' {
                        row += 2_isize.pow(6 - i as u32);
                    }
                } else {
                    if c == 'R' {
                        col += 2_isize.pow(2 - (i - 7) as u32);
                    }
                }
            }
            (row, col)
        })
        .collect();

        let ids: Vec<isize> = passes.iter().map(|(r, c)| r * 8 + c).collect();
        let max = ids.iter().max().unwrap();
        println!("Part 1: {}", max);

        // This is so slow
        for i in 1..*max {
            if !ids.contains(&i) && ids.contains(&(i + 1)) && ids.contains(&(i - 1)) {
                println!("Part 2: {}", i);
            }
        }
}
