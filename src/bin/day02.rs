use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let passwords: Vec<(usize, usize, char, String)> = stdin.lock().lines()
        .map(|l| {
            let parts: Vec<String> = l.unwrap().split_whitespace().map(|s| s.to_string()).collect();
            let reqs: Vec<usize> = parts[0].split('-').map(|n| n.parse::<usize>().unwrap()).collect();
            (reqs[0], reqs[1],
             parts[1].strip_suffix(':').unwrap().chars().nth(0).unwrap(),
             parts[2].clone())
        })
        .collect();

    let mut valid = 0;
    let mut valid2 = 0;
    for (lbnd, ubnd, letter, password) in passwords {
        let num = password.chars().filter(|c| *c == letter).count();
        if lbnd <= num && num <= ubnd {
            valid += 1;
        }
        if (password.chars().nth(lbnd - 1).unwrap() == letter ||
            password.chars().nth(ubnd - 1).unwrap() == letter) &&
            password.chars().nth(lbnd - 1).unwrap() != password.chars().nth(ubnd - 1).unwrap() {
                valid2 += 1;
            }
    }
    println!("Part 1: {}", valid);
    println!("Part 2: {}", valid2);
}
