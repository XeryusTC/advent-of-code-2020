use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let items: Vec<i32> = stdin.lock().lines()
        .map(|l| l.unwrap().parse::<i32>().unwrap())
        .collect();

    'outer: for a in items.iter() {
        for b in items.iter() {
            if a + b == 2020 {
                println!("Part 1: {}", a * b);
                break 'outer;
            }
        }
    }

    'outer: for a in items.iter() {
        for b in items.iter() {
            for c in items.iter() {
                if a + b + c == 2020 {
                    println!("Part 1: {}", a * b * c);
                    break 'outer;
                }
            }
        }
    }
}
